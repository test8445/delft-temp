#!/usr/bin/env python3

import requests

# Target URL
target = 'https://www.weerindelft.nl/clientraw.txt'


# Returns rounded temperature (str) or 'N/A' in case of an error in processing dowloaded data.
def get_temp(data):
    try:
        return str(round(float(data.split()[4])))
    except:
        return 'N/A'
		

if __name__ ==  '__main__':
    # Send a HTTP request and, if there are no errors, process and print result.
    try:
        res = requests.get(target)
    except:     # Catch a network problem and write an error message.
        print('Failed to connect')
    else:
        # Process data if HTTP code 200 is received. Otherwise print and error message. 
        if res.ok:  
            print(get_temp(res.text), 'degrees Celcius')
        else:
            print('Failed to download data')

